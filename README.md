# Python Programming

## Table of Contents
1. [Style Guide](#style-guide)
2. [Basic Syntax](#basic-syntax)
  - [Excuting Syntax](#excuting-syntax)
  - [Comments](#comments)
  - [Data Types](#data-types)
3. [Functions](#functions)
  - [Defining Functions](#defining-functions)
  - [Arguments and Parameters](#arguments-and-parameters)
  - [Lambda Functions](#lambda-functions)
  - [Variable Scope](#variable-scope)
    - [Global Variables](#global-variables)
    - [Local Variables](#local-variables)
    - [Nonlocal Keyword](#nonlocal-keyword)
  - [Advanced Function Topics](#advanced-function-topics)
    - [Decorators](#decorators)
4. [Data Structures](#data-structures)
  - [Lists](#lists)
  - [Tuples](#tuples)
  - [Dictionaries](#dictionaries)
  - [Sets](#sets)
  - [Copying Data Structures](#copying-data-structures)
    - [Shallow Copy](#shallow-copy)
    - [Deep Copy](#deep-copy)
5. [Modules and Packages](#modules-and-packages)
  - [Importing Modules](#importing-modules)
  - [Creating Modules](#creating-modules)
  - [Standard Library](#standard-library)
6. [File Handling](#file-handling)
  - [Reading Files](#reading-files)
  - [Writing Files](#writing-files)
  - [File Methods](#file-methods)
7. [Error Handling](#error-handling)
  - [Try, Except and Finally](#try-except-and-finally)
8. [Object-Oriented Programming](#object-oriented-programming)
  - [Classes and Objects](#classes-and-objects)
  - [Inheritance](#inheritance)
  - [Polymorphism](#polymorphism)
  - [Encapsulation](#encapsulation)
9. [Useful Libraries](#useful-libraries)
  - [NumPy](#numpy)
  - [Pandas](#pandas)
  - [Matplotlib](#matplotlib)
  - [Requests](#requests)

## Tutorial
[Python Tutorial](https://docs.python.org/3/tutorial/index.html)

## Style Guide
[PEP8 Style Guide](https://peps.python.org/pep-0008/)

## Basic Syntax
### Excuting Syntax
```
print('document', 'nyeom', sep='/')

print('I'm %s. i''m %d yesrs old' % ('rocio', 30) )

print('I'm {1} years old. my name is {0}'.format('rocio', 30))

person = { 'name': 'nayoon', 'age': 30 }

print('I'm {age} years old. my name is {name}'.format(name=person['name'], age=person['age']))

print('I'm {age} years old. my name is {name}'.format(**person))
```

### Comments
```
# comments
print('Hello World')
```

### Data Types
```
age: int = 0
name : str = 'Hello'
height: float = 6.0
is_student: bool = True
```

## Functions
### Defining Functions
```
def get_full_name(first_name: str, last_name:str) -> str: # type hing
  return f'{first_name} {last_name}'
print(test('rocio', 'eom')) # 'rocio eom'
```

### Arguments and Parameters
```
def unlimited_arguments(*args):
  print(args) # Tuple
unlimited_arguments(1,2,3)

def calc(**kwargs):
  print(kwargs) # Dictionary
calc(n1=1, n2=3, func='add')

def calc(func, **kwargs): # mandatory parameter (=func)
  for (k, v) in kwargs.items():
    print(k)
    print(v)
  if kwargs['func'] == 'add':
    return kwargs['n1'] + kwargs['n2']
calc(n1=1, n2=3, func='add')
```

### Lambda Functions
```
def squre(num):
  return num**2

square = lambda num: num**2
print(square(2))
```

### Variable Scope
#### Global Variables
#### Local Variables
#### Nonlocal Keyword
```
def func():
  x = 10
  def func2():
    nonlocal x
    x = 20
  func2()
  print(x) # 20

func()
```

### Advanced Function Topics
#### Decorators
```
def higer_order_example(func):
  def inside():
    print('start...')
    func()
    print('end...')
  return inside

@higer_order_example
def sample_example():
  print('i am inside')

sample_example()
```

## Data Structures
### Lists
```
str = 'apple'
str_list = list(str) # convert string to list

str_list = []
for idx in range(len(str)): 
  str_list.append(str[idx])

str_list = []
for char in str:
  str_list.append(char)
print(list)

str_list.reverse() # return None
print(str_list) # ['e', 'l', 'p', 'p', 'a']
print(list(reversed(str))) # ['e', 'l', 'p', 'p', 'a']
```

### Tuples
```
my_tuple = (1,2,3)
```

### Dictionaries
```
dict = {
  'country': 'south korea',
  'city': 'incheon',
}

for key, value in dict.items():
  print(f'{key} {value}')

for key in dict:
  print(f'{key} {dict[key]}')

print(list(dict.keys())) # convert dictionaries to lists
print(dict.values()) # get values of keys
print('state' in dict) # validate to check if the key value exists
 
```

### Sets
```
my_set1 = set((1,2,3))
my_set1 = {1, 2, 3}
print(my_set1)
my_set1.add(4)
print(my_set1)
my_set1.remove(4)
print(my_set1)
```

### Copying Data Structures
#### Shallow Copy
```
a = [[1, 2], 3]
b = a[:]
b.append(4)
b[0].append(3)
print(a)
print(b)
```

#### Deep Copy
```
import copy

a = [[1, 2], 3]
b = copy.deepcopy(a)
b.append(4)
b[0].append(3)
print(a)
print(b)
```

## Modules and Packages
### Importing Modules
```
from car import Car

tesla = Car(color='green', engine_type='hybrid')
print(tesla.color)
print(vars(tesla))
```

### Creating Modules
### Standard Library
#### datetime
```
import datetime

current = datetime.datetime.now()
custom_date = datetime.datetime(
  year=2024,
  month=1,
  day=1
)
print(current.weekday())

datetime_object = datetime.datetime.strptime('2024-05-14 00:00:00', '%Y-%m-%d %H:%M:%S') # convert string to datetime

datetime_string = datetime_object.strftime('%Y-%m-%d %H:%M:%S') # convert datetime to string

from datetime import timedelta
print(datetime_object + timedelta(days=1))
```

#### json
```
import json

with open('sample.json', mode='r') as f:
  data = json.loads(f.read())
  print(data) # dictionary
  with open('sample.json', mode='w') as w:
    w.write(json.dumps(data))
```

### csv
```
import csv
with open('sample.csv', 'r') as file:
    data = csv.reader(file)
    for row in data:
        print(row)
```

## File Handling
### Reading Files
```
# need to close file for memory management
file = open('sample.txt', 'r')
print(file.read())
file.close()

# no need to use close method
with open('sample.txt', 'r') as file:
    print(file.read())
```

### Writing Files
```
with open('sample.txt', 'w') as file:
   print(file.write('Thanks'))
```

## Error Handling
### Try, Except and Finally
```
# custom error
class MaximumNumberExceededException(Exception):
    def __init__(self):
        print('invalid number fomat')
    
try:
    grade = int(input('Type your socre from 0 to 100: '))
    if grade > 100:
        raise MaximumNumberExceededException()
except TypeError:
    print('TypeError occurs')
except MaximumNumberExceededException as e:
    pass
else:
    print('this got triggered due to no error found')
finally:
    print('always running this')
```

## Object-Oriented Programming
### Classes and Objects
```
class Car:
  def __init__(self, body_type):
    self.body_type = body_type
    
  def __repr__(self):
    return f'Car({self.body_type} body type)'
    
  def set_body_type(self, body_type):
    self.body_type = body_type
  @classmethod
  def hyundai(cls):
    return cls('sedan')

c = Car(body_type='sport')
c.set_body_type('sedan')
print(c)
print(Car.hyundai())

class Calc:
  def add(x: int, y: int) -> int:
    return x + y
Calc.add = staticmethod(Calc.add)
print(Calc.add(1,2))

class Calc2:
  @staticmethod
  def add(x: int, y: int) -> int:
    return x + y
print(Calc2.add(1,2))
```

### Inheritance
```
class Car:
  def __init__(self):
    self.wheel_count = 4
    self.door_count = 2
  def start(self):
    print('start')
  def drive(self):
    print('drive')

class ElectricCar(Car):
  def __init__(self):
    super().__init__()
  def start(self):
    super().start()
    print('no sound...')
```

### Polymorphism
### Encapsulation

## Useful Libraries
### Prettytable
[Prettytable]()
```
from prettytable import PrettyTable

table = PrettyTable()
table.field_names = ["City name", "Area", "Population", "Annual Rainfall"]
table.add_row(["Adelaide", 1295, 1158259, 600.5])
table.add_row(["Brisbane", 5905, 1857594, 1146.4])
table.add_row(["Darwin", 112, 120900, 1714.7])
table.add_row(["Hobart", 1357, 205556, 619.5])
table.add_row(["Sydney", 2058, 4336374, 1214.8])
table.add_row(["Melbourne", 1566, 3806092, 646.9])
table.add_row(["Perth", 5386, 1554769, 869.4])
print(table)
```

### NumPy
[NumPy](https://numpy.org/)
```
import numpy

avg = numpy.mean([1,2,3,4,5])
print(avg)
```

### Pandas
[Pandas](https://pandas.pydata.org/)
```
import pandas

data = pandas.read_csv('sample.csv)
print(type(data)) # <class 'pandas.core.frame.DataFrame'>
print(type(data['country'])) # <class 'pandas.core.series.Series'>
print(data.to_dict())
print(data.country.to_list())
print(data.population.sum())
print(data[data.country == 'South Korea'])
print(data[data.population == data.population.max()])

grade = {
  'student': ['a','b','c'],
  'scores': [30,40,50]
}
data = pandas.DataFrame(grade)
print(data)

score_dict = {
  'student': ['Tim','Lisa','Sarah'],
  'score': [80, 90, 95]
}
data = pandas.DataFrame(score_dict)
for (key, value) in data.items():
  print(key)
  print(value)
for (i, row) in data.iterrows():
  print(f'{row.student} : {row.score}')
#dictionary comprehension
data = {
  'tom': 80,
  'kate': 90
}
updated_data = { name: score + 5 for (name, score) in data.items() }
print(updated_data)
```

### Matplotlib
[Metplotlib](https://matplotlib.org/)

### Requests
[Requests: HTTPS for Humans](https://requests.readthedocs.io/en/latest/)
```
import requests

response= requests.get('http://api.open-notify.org/iss-now.json')
print(response) # 200 OK

response.raise_for_status() # 4XX or 5XX

payload = response.json() # payload
print(payload)
```